import React from 'react';
//import logo from './logo.svg';
import './App.css';

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <div className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h2>Welcome to React</h2>
//         </div>
//         <p className="App-intro">
//           To get started, edit <code>src/App.js</code> and save to reload.
//         </p>
//       </div>
//     );
//   }
// }

//export default App;

// Stateless functional component is used here since "Square" consists only of a render method
function Square(props) {
  return (
    <button className="square" onClick={() => props.onClick()}>
      {props.value}
    </button>
  );
}

class Board extends React.Component{
  constructor() {
    super();
    this.state = {
      squares: Array(9).fill(null),
      isX: true,
    };
  }

  renderSquare(i){
    return <Square value={this.state.squares[i]} onClick={() => this.handleClick(i)} />
  }

  // My solution
  // Just add {this.renderStatus()} in render method
  // renderStatus() {
  //   const status = 'Next player: ';
  //   return <div className="status">{status}{this.state.isX ? 'X' : 'O'}</div>
  // }

  render() {
    const winner = calculateWinner(this.state.squares);
    let status;
    if (winner) {
      status = 'winner: ' + winner;
    }
    else {
      status = 'Next player: ' + (this.state.isX ? 'X' : 'O');
    }

    return (
      <div>
        <div className="status">{status}{this.state.isX}</div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }

  handleClick(i) {
    // slice is used to copy the array prior to making changes and to prevent mutating the existing array
    const squares = this.state.squares.slice();
    squares[i] = this.state.isX ? 'X' : 'O';
    this.setState ({
        squares: squares,
        isX: !this.state.isX,
    });
    
    // My solution
    // this.setState({squares: squares});
    // this.state.isX ? 'X' : 'O';
  }

}

class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
            <Board />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

export default Game;
// ========================================

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}


