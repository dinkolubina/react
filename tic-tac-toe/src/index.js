import React from 'react';
import ReactDOM from 'react-dom';
//import App from './App';
import './index.css';

import Game from './App';

// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );

ReactDOM.render(
  <Game />,
  document.getElementById('container')
);